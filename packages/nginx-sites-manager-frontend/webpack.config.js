const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')

const production = process.env.NODE_ENV === 'production'

module.exports = {
  context: __dirname,

  mode: production ? 'production' : 'development',

  entry: path.resolve(__dirname, 'src/index.js'),

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'static/index.html'),
      alwaysWriteToDisk: true,
      minify: production && {
        collapseWhitespace: true
      }
    }),
    new HtmlWebpackHarddiskPlugin()
  ],

  resolve: {
    alias: {
      'src': path.resolve(__dirname, 'src')
    }
  },

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    historyApiFallback: {
      index: 'index.html'
    }
  }
}
