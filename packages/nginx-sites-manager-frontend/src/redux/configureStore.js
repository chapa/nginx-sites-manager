import { applyMiddleware, createStore } from 'redux'
import { apiMiddleware } from 'redux-api-middleware'

import { apiJsonBodyHandler } from './middlewares'
import reducer from './modules'

const createStoreWithMiddleware = applyMiddleware(
  apiJsonBodyHandler,
  apiMiddleware
)(createStore)

export default () => createStoreWithMiddleware(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
