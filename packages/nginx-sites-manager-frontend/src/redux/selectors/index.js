export const getSites = state => state.sites
export const getTemplates = state => state.templates
export const getTemplate = (state, { name }) => state.templates[name]
