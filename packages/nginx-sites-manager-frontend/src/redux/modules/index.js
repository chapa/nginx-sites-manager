import { combineReducers } from 'redux'

import sites from './sites'
import templates from './templates'

// Reducer

export default combineReducers({
  sites,
  templates
})
