import { RSAA } from 'redux-api-middleware'

// Actions

const PREFIX = 'SITES'
const LOAD_ALL = {
  REQUEST: `${PREFIX}/LOAD_ALL/REQUEST`,
  SUCCESS: `${PREFIX}/LOAD_ALL/SUCCESS`,
  FAILURE: `${PREFIX}/LOAD_ALL/FAILURE`
}
const UPDATE = {
  REQUEST: `${PREFIX}/UPDATE/REQUEST`,
  SUCCESS: `${PREFIX}/UPDATE/SUCCESS`,
  FAILURE: `${PREFIX}/UPDATE/FAILURE`
}

// Reducer

export default (state = {}, action = {}) => {
  switch (action.type) {
    case LOAD_ALL.SUCCESS:
      return Object.assign({}, ...action.payload.map(site => ({
        [site.name]: site
      })))

    case UPDATE.SUCCESS:
      return {
        ...state,
        [action.payload.name]: action.payload
      }
  }

  return state
}

// Action Creators

export const loadAll = () => ({
  [RSAA]: {
    endpoint: 'http://localhost:3000/sites',
    method: 'GET',
    types: [LOAD_ALL.REQUEST, LOAD_ALL.SUCCESS, LOAD_ALL.FAILURE]
  }
})

export const update = (name, payload) => ({
  [RSAA]: {
    endpoint: `http://localhost:3000/sites/${name}`,
    method: 'PATCH',
    body: payload,
    types: [UPDATE.REQUEST, UPDATE.SUCCESS, UPDATE.FAILURE]
  }
})
