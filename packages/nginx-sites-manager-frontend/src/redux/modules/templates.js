import { RSAA } from 'redux-api-middleware'

// Actions

const PREFIX = 'TEMPLATES'
const LOAD_ALL = {
  REQUEST: `${PREFIX}/LOAD_ALL/REQUEST`,
  SUCCESS: `${PREFIX}/LOAD_ALL/SUCCESS`,
  FAILURE: `${PREFIX}/LOAD_ALL/FAILURE`
}
const CREATE = {
  REQUEST: `${PREFIX}/CREATE/REQUEST`,
  SUCCESS: `${PREFIX}/CREATE/SUCCESS`,
  FAILURE: `${PREFIX}/CREATE/FAILURE`
}
const UPDATE = {
  REQUEST: `${PREFIX}/UPDATE/REQUEST`,
  SUCCESS: `${PREFIX}/UPDATE/SUCCESS`,
  FAILURE: `${PREFIX}/UPDATE/FAILURE`
}

// Reducer

export default (state = {}, action = {}) => {
  switch (action.type) {
    case LOAD_ALL.SUCCESS:
      return Object.assign({}, ...action.payload.map(template => ({
        [template.name]: template
      })))

    case CREATE.SUCCESS:
      return {
        ...state,
        [action.payload.name]: action.payload
      }

    case UPDATE.SUCCESS:
      return {
        ...(action.payload.name === action.meta.name ? (
          state
        ) : (
          // If name has changed (= template has been renamed), the old one must be removed
          Object.assign({}, ...Object.keys(state)
            .filter(name => name !== action.meta.name)
            .map(name => ({
              [name]: state[name]
            })))
        )),
        [action.payload.name]: action.payload
      }
  }

  return state
}

// Action Creators

export const loadAll = () => ({
  [RSAA]: {
    endpoint: 'http://localhost:3000/templates',
    method: 'GET',
    types: [LOAD_ALL.REQUEST, LOAD_ALL.SUCCESS, LOAD_ALL.FAILURE]
  }
})

export const create = payload => ({
  [RSAA]: {
    endpoint: `http://localhost:3000/templates`,
    method: 'POST',
    body: payload,
    types: [CREATE.REQUEST, CREATE.SUCCESS, CREATE.FAILURE]
  }
})

export const update = (name, payload) => ({
  [RSAA]: {
    endpoint: `http://localhost:3000/templates/${name}`,
    method: 'PATCH',
    body: payload,
    types: [UPDATE.REQUEST, UPDATE.SUCCESS, UPDATE.FAILURE].map(type => ({
      type,
      meta: { name }
    }))
  }
})
