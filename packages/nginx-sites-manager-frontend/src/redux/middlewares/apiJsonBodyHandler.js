import { RSAA } from 'redux-api-middleware'

export default store => next => action => {
  const apiCall = action[RSAA]

  if (apiCall && apiCall.body && typeof apiCall.body !== 'string') {
    const { headers } = apiCall

    apiCall.headers = state => ({
      'Content-Type': 'application/json',
      ...(typeof headers === 'function' ? headers(state) : headers)
    })

    apiCall.body = JSON.stringify(apiCall.body)
  }

  return next(action)
}
