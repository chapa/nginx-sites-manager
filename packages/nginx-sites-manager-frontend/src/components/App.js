import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { hot } from 'react-hot-loader'

import { MuiThemeProvider } from '@material-ui/core/styles'

import createTheme from '../createTheme'
import Layout from './Layout'
import Router from './Router'

const theme = createTheme()

class App extends Component {
  render () {
    return (
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <Layout>
            <Router />
          </Layout>
        </BrowserRouter>
      </MuiThemeProvider>
    )
  }
}

export default hot(module)(App)
