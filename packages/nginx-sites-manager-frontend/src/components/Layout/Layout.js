import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import CssBaseline from '@material-ui/core/CssBaseline'

import Sidemenu from './components/Sidemenu'
import Topbar from './components/Topbar'

import styles from './Layout.styles'

class Layout extends Component {
  state = {
    sidemenuOpen: false
  }

  handleSideMenuOpen = () => {
    this.setState({
      sidemenuOpen: true
    })
  }

  handleSideMenuClose = () => {
    this.setState({
      sidemenuOpen: false
    })
  }

  render () {
    const { children, classes } = this.props
    const { sidemenuOpen } = this.state

    return (
      <React.Fragment>
        <CssBaseline />
        <Sidemenu
          onClose={this.handleSideMenuClose}
          open={sidemenuOpen}
        />
        <Topbar
          onMenuButtonClick={this.handleSideMenuOpen}
          sidemenuOpen={sidemenuOpen}
        />
        <div className={classes.content}>
          {children}
        </div>
      </React.Fragment>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node
}

export default withStyles(styles)(Layout)
