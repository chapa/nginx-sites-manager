export default theme => ({
  content: {
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.sidemenu.width
    },

    marginTop: 56,
    [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
      marginTop: 48
    },
    [theme.breakpoints.up('sm')]: {
      marginTop: 64
    },

    padding: 16
  }
})
