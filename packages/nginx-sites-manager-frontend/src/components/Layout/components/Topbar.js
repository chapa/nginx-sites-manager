import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'

import AppBar from '@material-ui/core/AppBar'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import styles from './Topbar.styles'

const titles = {
  sites: () => 'Sites',
  templates: () => 'Templates'
}

class Topbar extends Component {
  render () {
    const { classes, onMenuButtonClick } = this.props

    return (
      <AppBar position='fixed' className={classes.root}>
        <Toolbar>
          <Hidden mdUp implementation='css'>
            <IconButton className={classes.menuButton} color='inherit' onClick={onMenuButtonClick}>
              <MenuIcon />
            </IconButton>
          </Hidden>
          <Typography variant='headline' color='inherit' className={classes.flexGrow}>
            <Switch>
              <Route path='/sites' component={titles.sites} />
              <Route path='/templates' component={titles.templates} />
            </Switch>
          </Typography>
        </Toolbar>
      </AppBar>
    )
  }
}

Topbar.propTypes = {
  onMenuButtonClick: PropTypes.func
}

export default withStyles(styles)(Topbar)
