export default theme => ({
  root: {
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${theme.sidemenu.width}px)`
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
})
