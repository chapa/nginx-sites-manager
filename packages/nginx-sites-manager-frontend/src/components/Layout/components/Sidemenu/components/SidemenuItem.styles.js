export default theme => ({
  subheading: {
    color: 'inherit',
    fontWeight: 'inherit'
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightMedium
  }
})
