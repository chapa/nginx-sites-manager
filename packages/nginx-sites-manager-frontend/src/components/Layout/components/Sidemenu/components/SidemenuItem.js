import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'

import ListItem from '@material-ui/core/ListItem'
import Typography from '@material-ui/core/Typography'

import styles from './SidemenuItem.styles'

class SidemenuItem extends Component {
  render () {
    const { classes, label, to } = this.props

    return (
      <ListItem button component={NavLink} to={to} activeClassName={classes.active}>
        <Typography variant='subheading' classes={{ subheading: classes.subheading }}>
          {label}
        </Typography>
      </ListItem>
    )
  }
}

SidemenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired
}

export default withStyles(styles)(SidemenuItem)
