import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import styles from './Sidemenu.styles'
import SidemenuItem from './components/SidemenuItem'

class Sidemenu extends Component {
  render () {
    const { classes, onClose, open } = this.props

    const content = (
      <div>
        <Toolbar>
          <Typography variant='title'>
            Nginx Sites Manager
          </Typography>
        </Toolbar>
        <Divider />
        <List component='nav'>
          <SidemenuItem label='Sites' to='/sites' />
          <SidemenuItem label='Templates' to='/templates' />
        </List>
      </div>
    )

    return (
      <React.Fragment>
        <Hidden mdUp implementation='css'>
          <Drawer
            variant='temporary'
            open={open}
            onClose={onClose}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true
            }}
          >
            {content}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation='css'>
          <Drawer
            variant='permanent'
            open
            classes={{
              paper: classes.drawerPaper
            }}
          >
            {content}
          </Drawer>
        </Hidden>
      </React.Fragment>
    )
  }
}

Sidemenu.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func
}

export default withStyles(styles)(Sidemenu)
