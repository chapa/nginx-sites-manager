export default theme => ({
  drawerPaper: {
    width: theme.sidemenu.width
  },
  link: {
    color: 'inherit',
    fontWeight: 'inherit'
  },
  activeLink: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightMedium
  }
})
