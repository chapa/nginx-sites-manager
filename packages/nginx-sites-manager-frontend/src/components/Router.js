import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import Sites from '../scenes/Sites'
import Templates from '../scenes/Templates'

class Router extends Component {
  render () {
    return (
      <Switch>
        <Route path='/sites' component={Sites} />
        <Route path='/templates' component={Templates} />
        <Redirect to='/sites' />
      </Switch>
    )
  }
}

export default Router
