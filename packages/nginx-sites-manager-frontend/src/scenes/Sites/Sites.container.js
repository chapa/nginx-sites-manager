import { connect } from 'react-redux'

import { loadAll } from '../../redux/modules/sites'
import { getSites } from '../../redux/selectors'

import Sites from './Sites'

const mapStateToProps = state => ({
  sites: getSites(state)
})

const mapDispatchToProps = {
  loadAll
}

export default connect(mapStateToProps, mapDispatchToProps)(Sites)
