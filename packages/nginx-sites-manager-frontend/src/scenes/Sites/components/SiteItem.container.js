import { connect } from 'react-redux'

import { update } from '../../../redux/modules/sites'

import SiteItem from './SiteItem'

const mapDispatchToProps = {
  update
}

export default connect(undefined, mapDispatchToProps)(SiteItem)
