import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/monokai.css'
import 'codemirror/mode/nginx/nginx'

import PropTypes from 'prop-types'
import React, { Component } from 'react'
import TimeAgo from 'react-timeago'
import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import Collapse from '@material-ui/core/Collapse'
import { Controlled as CodeMirror } from 'react-codemirror2'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import Switch from '@material-ui/core/Switch'
import Typography from '@material-ui/core/Typography'

import styles from './SiteItem.styles'

class SiteItem extends Component {
  state = {
    draftContent: this.props.site.content,
    expanded: false
  }

  stopPropagation = e => e.stopPropagation()

  handlePanelExpansion = () => {
    this.setState(({ expanded }) => ({
      expanded: !expanded
    }))
  }

  handleToggleEnabled = e => {
    const { site: { name }, update } = this.props

    update(name, {
      enabled: e.target.checked
    })
  }

  handleContentChange = (editor, data, value) => {
    this.setState({
      draftContent: value
    })
  }

  resetDraftContent = () => {
    this.setState({
      draftContent: this.props.site.content
    })
  }

  saveDraftContent = () => {
    const { site: { name }, update } = this.props

    update(name, {
      content: this.state.draftContent
    })
  }

  render () {
    const { classes, site: { content, enabled, lastModified, name } } = this.props
    const { draftContent, expanded } = this.state
    const dateLastModified = new Date(lastModified)

    return (
      <ExpansionPanel expanded={expanded} onChange={this.handlePanelExpansion} CollapseProps={{ mountOnEnter: true }}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Switch
            color='primary'
            checked={enabled}
            onClick={this.stopPropagation}
            onChange={this.handleToggleEnabled}
            classes={{
              root: classes.switch,
              switchBase: classes.switchBase
            }}
          />
          <Typography variant={draftContent === content ? 'body1' : 'body2'} className={classes.heading}>
            {draftContent !== content && '* '}{name}
          </Typography>
          <Typography variant={draftContent === content ? 'body1' : 'body2'} className={classes.secondaryHeading}>
            <TimeAgo date={dateLastModified} title={dateLastModified.toLocaleString()} />
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails classes={{ root: classes.expansionPanelCode }}>
          <CodeMirror
            className={classes.codeMirror}
            value={draftContent}
            options={{
              mode: 'nginx',
              theme: 'monokai',
              lineNumbers: true
            }}
            onBeforeChange={this.handleContentChange}
          />
        </ExpansionPanelDetails>
        <Collapse in={draftContent !== content}>
          <ExpansionPanelActions classes={{ root: classes.expansionPanelActions }}>
            <Button size='small' onClick={this.resetDraftContent}>
              Cancel
            </Button>
            <Button size='small' color='primary' onClick={this.saveDraftContent}>
              Save
            </Button>
          </ExpansionPanelActions>
        </Collapse>
      </ExpansionPanel>
    )
  }
}

SiteItem.propTypes = {
  site: PropTypes.shape({
    content: PropTypes.string.isRequired,
    enabled: PropTypes.bool.isRequired,
    lastModified: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  update: PropTypes.func.isRequired
}

export default withStyles(styles)(SiteItem)
