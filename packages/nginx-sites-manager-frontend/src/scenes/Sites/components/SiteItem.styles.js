export default theme => ({
  switch: {
    width: 36,
    marginRight: 8,
    transform: 'translateX(-8px)'
  },
  switchBase: {
    width: 22,
    height: 'auto'
  },
  heading: {
    flexBasis: '50%'
  },
  secondaryHeading: {
    color: theme.palette.text.secondary
  },
  expansionPanelCode: {
    padding: 0
  },
  codeMirror: {
    width: '100%'
  },
  expansionPanelActions: {
    padding: 8
  }
})
