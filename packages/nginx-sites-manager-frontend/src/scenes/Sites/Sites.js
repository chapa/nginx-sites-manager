import PropTypes from 'prop-types'
import React, { Component } from 'react'

import Grid from '@material-ui/core/Grid'

import { SiteItem } from './components'

class Sites extends Component {
  componentDidMount () {
    this.props.loadAll()
  }

  render () {
    const { sites } = this.props

    return (
      <Grid container spacing={16}>
        {Object.keys(sites).sort().map(name => (
          <Grid key={name} item xs={12} lg={6} xl={4}>
            <SiteItem site={sites[name]} />
          </Grid>
        ))}
      </Grid>
    )
  }
}

Sites.propTypes = {
  loadAll: PropTypes.func.isRequired
}

export default Sites
