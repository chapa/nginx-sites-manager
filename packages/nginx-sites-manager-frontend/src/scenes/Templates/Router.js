import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import TemplatesList from './scenes/TemplatesList'
import TemplateForm from './scenes/TemplateForm'

class Router extends Component {
  render () {
    return (
      <Switch>
        <Route path='/templates' exact component={TemplatesList} />
        <Route path='/templates/new' exact component={TemplateForm} />
        <Route path='/templates/:name' component={TemplateForm} />
        <Redirect to='/templates' />
      </Switch>
    )
  }
}

export default Router
