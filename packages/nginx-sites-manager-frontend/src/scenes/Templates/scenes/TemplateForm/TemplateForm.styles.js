export default theme => ({
  root: {
    maxWidth: theme.breakpoints.width('md'),
    margin: 'auto'
  },
  codeMirror: {
    '& .CodeMirror': {
      height: 'auto'
    }
  },
  actions: {
    justifyContent: 'flex-end'
  }
})
