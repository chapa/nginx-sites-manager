import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'

class TextFieldAdapter extends Component {
  render () {
    const {
      input: { name, onChange, value, ...otherInputProps },
      meta,
      ...otherProps
    } = this.props

    return (
      <TextField
        {...otherProps}
        name={name}
        helperText={meta.touched ? meta.error : undefined}
        error={meta.error && meta.touched}
        inputProps={otherInputProps}
        onChange={onChange}
        value={value}
      />
    )
  }
}

export default TextFieldAdapter
