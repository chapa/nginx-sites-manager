import React, { Component } from 'react'

import { Controlled as CodeMirror } from 'react-codemirror2'

class CodeMirrorAdapter extends Component {
  handleBeforeChange = (a, b, value) => {
    this.props.input.onChange(value)
  }

  render () {
    const {
      input: { value },
      meta,
      ...otherProps
    } = this.props

    return (
      <CodeMirror
        {...otherProps}
        onBeforeChange={this.handleBeforeChange}
        value={value}
      />
    )
  }
}

export default CodeMirrorAdapter
