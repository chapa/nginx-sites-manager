import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import isEqual from 'fast-deep-equal'
import Mustache from 'mustache'

import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CodeMirror from './components/CodeMirrorAdapter'
import { Field, Form } from 'react-final-form'
import Grid from '@material-ui/core/Grid'
import TextField from './components/TextFieldAdapter'
import Typography from '@material-ui/core/Typography'

import styles from './TemplateForm.styles'

class TemplateForm extends Component {
  handleSubmit = template => {
    const { onChange, template: initialValues } = this.props

    const payload = Object.assign({}, ...Object.keys(template)
      .filter(key => !isEqual(template[key], initialValues[key]))
      .map(key => ({
        [key]: template[key]
      }))
    )

    if (payload.defaultValues === undefined) {
      // Little hack to fix a strange behavior when erasing a default value which was alone
      payload.defaultValues = {}
    }

    onChange(payload)
  }

  render () {
    const { classes, showResetButton, template } = this.props

    return (
      <Form
        onSubmit={this.handleSubmit}
        initialValues={template}
        render={({ handleSubmit, form: { reset }, pristine, values }) => {
          const tokens = Array.from(new Set(Mustache.parse(values.content)
            .filter(token => token[0] === 'name')
            .map(token => token[1])
          ))

          return (
            <form onSubmit={handleSubmit}>
              <Card className={classes.root}>
                <CardContent>
                  <Field
                    name='name'
                    component={TextField}
                    type='text'
                    label='Name'
                    fullWidth
                  />
                </CardContent>
                <Field
                  name='content'
                  component={CodeMirror}
                  className={classes.codeMirror}
                  options={{
                    mode: 'nginx',
                    theme: 'monokai',
                    lineNumbers: true,
                    viewportMargin: Infinity,
                    indentUnit: 4,
                    extraKeys: {
                      Tab: cm => {
                        var spaces = Array(cm.getOption('indentUnit') + 1).join(' ')
                        cm.replaceSelection(spaces)
                      }
                    }
                  }}
                />
                {tokens.length > 0 && (
                  <CardContent>
                    <Typography variant='title'>Default values</Typography>
                    <Grid container spacing={16}>
                      {tokens.map(token => (
                        <Grid item key={token} xs={12} sm={6} md={4}>
                          <Field
                            name={`defaultValues.${token}`}
                            component={TextField}
                            type='text'
                            label={token}
                            fullWidth
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </CardContent>
                )}
                <CardActions className={classes.actions}>
                  {showResetButton && (
                    <Button
                      size='small'
                      onClick={reset}
                      disabled={pristine}
                    >
                      Reset
                    </Button>
                  )}
                  <Button
                    type='submit'
                    size='small'
                    color='primary'
                    disabled={pristine}
                  >
                    Save
                  </Button>
                </CardActions>
              </Card>
            </form>
          )
        }}
      />
    )
  }
}

TemplateForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  showResetButton: PropTypes.bool,
  template: PropTypes.shape({
    name: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
  }).isRequired
}

export default withStyles(styles)(TemplateForm)
