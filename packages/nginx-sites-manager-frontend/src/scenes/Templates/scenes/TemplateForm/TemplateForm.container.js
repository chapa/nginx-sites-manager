import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { create, loadAll, update } from '../../../../redux/modules/templates'
import { getTemplate } from '../../../../redux/selectors'

import TemplateForm from './TemplateForm'

const emptyTemplate = {
  name: '',
  content:
`server {
    listen {{ port }};
    listen [::]:{{ port }};

    root {{ project_path }};
    index index.html index.htm;

    server_name {{ server_name }};

    location / {
        try_files $uri $uri/ =404;
    }
}
`
}

class TemplateFormContainer extends Component {
  componentDidMount () {
    const { loadAll, match: { params: { name } }, template } = this.props

    if (name !== 'new' && template === undefined) {
      loadAll()
    }
  }

  handleChange = payload => {
    const {
      create,
      history,
      match: {
        params: { name },
        path
      },
      update
    } = this.props

    if (path === '/templates/new') {
      create({
        content: emptyTemplate.content,
        ...payload
      })
    } else {
      update(name, payload)
    }

    history.replace('/templates')
  }

  render () {
    const { match: { path }, template } = this.props
    const creation = path === '/templates/new'

    return (creation || template !== undefined) && (
      <TemplateForm template={template || emptyTemplate} onChange={this.handleChange} showResetButton={!creation} />
    )
  }
}

TemplateFormContainer.propTypes = {
  match: PropTypes.shape({
    path: PropTypes.string.isRequired,
    params: PropTypes.shape({
      name: PropTypes.string
    }).isRequired
  }).isRequired,
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired
  }).isRequired
}

const mapStateToProps = (state, { match: { params: { name } } }) => ({
  template: getTemplate(state, { name })
})

const mapDispatchToProps = {
  create,
  loadAll,
  update
}

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(TemplateFormContainer)
