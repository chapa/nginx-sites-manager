import { connect } from 'react-redux'

import { loadAll } from '../../../../redux/modules/templates'
import { getTemplates } from '../../../../redux/selectors'

import TemplatesList from './TemplatesList'

const mapStateToProps = state => ({
  templates: getTemplates(state)
})

const mapDispatchToProps = {
  loadAll
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplatesList)
