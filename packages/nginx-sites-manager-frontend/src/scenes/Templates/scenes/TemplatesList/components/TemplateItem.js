import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/monokai.css'
import 'codemirror/mode/nginx/nginx'

import PropTypes from 'prop-types'
import React, { Component } from 'react'
import TimeAgo from 'react-timeago'
import { withStyles } from '@material-ui/core/styles'

import AddIcon from '@material-ui/icons/Add'
import ButtonBase from '@material-ui/core/ButtonBase'
import IconButton from '@material-ui/core/IconButton'
import { Link } from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'

import styles from './TemplateItem.styles'

class TemplateItem extends Component {
  handleAddButtonClick = e => {
    e.preventDefault()
    window.alert('A dialog form is wanted here...')
  }

  render () {
    const { classes, template: { lastModified, name } } = this.props
    const dateLastModified = new Date(lastModified)

    return (
      <Paper elevation={1} className={classes.root}>
        <ButtonBase
          component={Link}
          to={`/templates/${name}`}
          disableRipple
          className={classes.buttonBase}
        >
          <Typography className={classes.heading}>
            {name}
          </Typography>
          <Typography className={classes.secondaryHeading}>
            <TimeAgo date={dateLastModified} title={dateLastModified.toLocaleString()} />
          </Typography>
          <div className={classes.actions}>
            <Tooltip title='New site with this template' enterDelay={500}>
              <IconButton className={classes.actionButton} onClick={this.handleAddButtonClick}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          </div>
        </ButtonBase>
      </Paper>
    )
  }
}

TemplateItem.propTypes = {
  template: PropTypes.shape({
    lastModified: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired
}

export default withStyles(styles)(TemplateItem)
