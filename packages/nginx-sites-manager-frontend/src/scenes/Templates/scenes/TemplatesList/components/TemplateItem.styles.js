export default theme => ({
  root: {
    borderRadius: 2
  },
  buttonBase: {
    width: '100%',
    height: 48,
    display: 'flex',
    justifyContent: 'flex-start',
    paddingLeft: 16
  },
  heading: {
    flexBasis: '50%'
  },
  secondaryHeading: {
    color: theme.palette.text.secondary
  },
  actions: {
    position: 'absolute',
    top: '50%',
    right: 8,
    transform: 'translateY(-50%)',
    '&:hover $actionButton': {
      backgroundColor: 'transparent'
    }
  },
  actionButton: {}
})
