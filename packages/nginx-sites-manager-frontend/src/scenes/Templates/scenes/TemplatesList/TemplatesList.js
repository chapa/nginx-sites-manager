import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import { Link } from 'react-router-dom'

import styles from './TemplatesList.styles'
import { TemplateItem } from './components'

class TemplatesList extends Component {
  componentDidMount () {
    this.props.loadAll()
  }

  render () {
    const { classes, templates } = this.props

    return (
      <React.Fragment>
        <div className={classes.addTemplateButtonWrapper}>
          <Button variant='outlined' color='primary' component={Link} to='/templates/new'>
            New template
          </Button>
        </div>
        <Grid container spacing={16}>
          {Object.keys(templates).sort().map(name => (
            <Grid key={name} item xs={12} lg={6} xl={4}>
              <TemplateItem template={templates[name]} />
            </Grid>
          ))}
        </Grid>
      </React.Fragment>
    )
  }
}

TemplatesList.propTypes = {
  loadAll: PropTypes.func.isRequired
}

export default withStyles(styles)(TemplatesList)
