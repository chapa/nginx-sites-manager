import fs from 'fs'
import path from 'path'

import { readFile, writeFile } from '../utils/file'

const { SITES_TEMPLATES_DIR } = process.env

export const getAll = ctx => {
  const sitesTemplates = fs.readdirSync(SITES_TEMPLATES_DIR).map(filename => {
    const realpath = fs.realpathSync(path.join(SITES_TEMPLATES_DIR, filename))

    const { metadata: { defaultValues }, content } = readFile(realpath)

    return {
      name: filename,
      content,
      defaultValues,
      lastModified: fs.statSync(realpath).mtime
    }
  })

  ctx.body = sitesTemplates
}

export const create = ctx => {
  const { content, defaultValues, name } = ctx.request.body

  if (content === undefined || name === undefined) {
    ctx.status = 400
    ctx.body = 'The request is mal formed'
    return
  }

  const filepath = path.join(SITES_TEMPLATES_DIR, name)

  if (fs.existsSync(filepath)) {
    ctx.status = 409
    ctx.body = 'This template already exists'
    return
  }

  writeFile(filepath, {
    metadata: Object.keys(defaultValues).length > 0 ? { defaultValues } : undefined,
    content
  })

  ctx.status = 201
  ctx.body = {
    name,
    content,
    defaultValues,
    lastModified: fs.statSync(filepath).mtime
  }
}

export const update = ctx => {
  let { name } = ctx.params
  const {
    content: newContent,
    defaultValues: newDefaultValues,
    name: newName
  } = ctx.request.body

  // Rename file if it's necessary
  if (newName !== undefined && newName !== name) {
    fs.renameSync(
      fs.realpathSync(path.join(SITES_TEMPLATES_DIR, name)),
      path.join(SITES_TEMPLATES_DIR, newName)
    )

    name = newName
  }

  const realpath = fs.realpathSync(path.join(SITES_TEMPLATES_DIR, name))
  let { metadata: { defaultValues }, content } = readFile(realpath)

  // Edit file if either content of default values has changed
  if (newContent !== undefined || newDefaultValues !== undefined) {
    if (newContent !== undefined) {
      content = newContent
    }

    if (newDefaultValues !== undefined) {
      defaultValues = newDefaultValues
    }

    writeFile(realpath, {
      metadata: Object.keys(defaultValues).length > 0 ? { defaultValues } : undefined,
      content
    })
  }

  ctx.body = {
    name: newName || name,
    content,
    defaultValues,
    lastModified: fs.statSync(realpath).mtime
  }
}
