import fs from 'fs'
import path from 'path'

import { toggleLinkIfNecessary } from '../utils/symlink'

const { SITES_AVAILABLE_DIR, SITES_ENABLED_DIR } = process.env

export const getAll = ctx => {
  const sitesEnabled = Object.assign({}, ...fs.readdirSync(SITES_ENABLED_DIR).map(filename => ({
    [fs.realpathSync(path.join(SITES_ENABLED_DIR, filename))]: {
      name: filename
    }
  })))

  const sitesAvailable = fs.readdirSync(SITES_AVAILABLE_DIR).map(filename => {
    const realpath = fs.realpathSync(path.join(SITES_AVAILABLE_DIR, filename))

    return {
      name: filename,
      content: fs.readFileSync(realpath).toString(),
      lastModified: fs.statSync(realpath).mtime,
      enabled: realpath in sitesEnabled
    }
  })

  ctx.body = sitesAvailable
}

export const update = ctx => {
  const { name } = ctx.params
  const { content, enabled } = ctx.request.body
  const realpath = fs.realpathSync(path.join(SITES_AVAILABLE_DIR, name))

  if (enabled !== undefined) {
    toggleLinkIfNecessary(name, enabled)
  }

  if (content !== undefined) {
    fs.writeFileSync(realpath, content)
  }

  const sitesEnabled = Object.assign({}, ...fs.readdirSync(SITES_ENABLED_DIR).map(filename => ({
    [fs.realpathSync(path.join(SITES_ENABLED_DIR, filename))]: {
      name: filename
    }
  })))

  ctx.body = {
    name,
    content: fs.readFileSync(realpath).toString(),
    lastModified: fs.statSync(realpath).mtime,
    enabled: realpath in sitesEnabled
  }
}
