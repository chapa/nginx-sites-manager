import Router from 'koa-router'

import { sites, templates } from './controllers'

const router = new Router()

router
  .get('/sites', sites.getAll)
  .patch('/sites/:name', sites.update)
  .get('/templates', templates.getAll)
  .post('/templates', templates.create)
  .patch('/templates/:name', templates.update)

export default router
