import 'dotenv/config'
import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import cors from '@koa/cors'

import router from './router'

const { APP_PORT } = process.env

const app = new Koa()

app.use(cors())
app.use(bodyParser())
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(APP_PORT).on('listening', () => {
  console.log(`Server is running at http://localhost:${APP_PORT}`)
})
