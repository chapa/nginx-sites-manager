import fs from 'fs'
import path from 'path'

const { SITES_AVAILABLE_DIR, SITES_ENABLED_DIR } = process.env

export const toggleLinkIfNecessary = (name, shouldExist) => {
  const sitesEnabled = Object.assign({}, ...fs.readdirSync(SITES_ENABLED_DIR).map(filename => ({
    [fs.realpathSync(path.join(SITES_ENABLED_DIR, filename))]: {
      name: filename
    }
  })))

  const linkpath = path.join(SITES_ENABLED_DIR, name)
  const linkExists = fs.existsSync(linkpath)
  const realpath = linkExists ? fs.realpathSync(linkpath) : path.join(SITES_AVAILABLE_DIR, name)

  if (linkExists && realpath in sitesEnabled && !shouldExist) {
    fs.unlinkSync(linkpath)
  } else if (!(realpath in sitesEnabled) && shouldExist) {
    fs.symlinkSync(realpath, linkpath)
  }

  return shouldExist
}
