import { EOL } from 'os'
import fs from 'fs'

const METADATA_PREFIX = '# nginx-sites-manager '

export const readFile = path => {
  const content = fs.readFileSync(path).toString()

  const contentArray = content.split(EOL)
  const matches = new RegExp(`^${METADATA_PREFIX}(.+)`).exec(contentArray[0])

  if (matches !== null) {
    do {
      contentArray.shift()
    } while (contentArray[0].search(/^\s*$/) !== -1)

    try {
      return {
        metadata: JSON.parse(matches[1]),
        content: contentArray.join(EOL)
      }
    } catch (e) {
      // matches[1] is not a valid JSON, original content will be returned
    }
  }

  return {
    metadata: {},
    content
  }
}

export const writeFile = (path, { metadata, content }) => {
  if (metadata !== undefined) {
    fs.writeFileSync(path, `${METADATA_PREFIX}${JSON.stringify(metadata)}\n\n${content}`)
  } else {
    fs.writeFileSync(path, content)
  }
}
